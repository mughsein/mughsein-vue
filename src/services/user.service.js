import axios from 'axios';
import authHeader from './auth-header';

// const DEV_URL = 'http://localhost:8080/api/test/';
const DEV_URL = 'http://localhost:3080/api/';
const PROD_URL = 'https://mughsein-express.herokuapp.com/api/';
const API_URL = (process.env.NODE_ENV == 'development' ? DEV_URL : PROD_URL);

class UserService {
  test(uname) {
    return uname
    // return axios.get(API_URL + 'profile/' + uname, { headers: authHeader() });
  }

  saveProfile(profile) {
    return axios
      .put(API_URL + 'profile/update', profile, { headers: authHeader() })
      .then(response => {
        if (response.data.accessToken) {
          localStorage.setItem('user', JSON.stringify(response.data));
        }

        return response.data;
      });
  }

  getProfile(uname) {
    return axios.get(API_URL + 'profile/' + uname, { headers: authHeader() });
  }

  getPublicContent() {
    return axios.get(API_URL + 'test/all');
  }

  getUserBoard() {
    return axios.get(API_URL + 'profile/all', { headers: authHeader() });
  }

  getModeratorBoard() {
    return axios.get(API_URL + 'test/mod', { headers: authHeader() });
  }

  getAdminBoard() {
    return axios.get(API_URL + 'test/admin', { headers: authHeader() });
  }
}

export default new UserService();
