import axios from 'axios';

// const DEV_URL = 'http://localhost:8080/api/auth/';
const DEV_URL = 'http://localhost:3080/api/auth/';
const PROD_URL = 'https://mughsein-express.herokuapp.com/api/auth/';
const API_URL = (process.env.NODE_ENV == 'development' ? DEV_URL : PROD_URL);

class AuthService {
  login(user) {
    return axios
      .post(API_URL + 'signin', {
        username: user.username,
        password: user.password
      })
      .then(response => {
        if (response.data.accessToken) {
          localStorage.setItem('user', JSON.stringify(response.data));
        }

        return response.data;
      });
  }

  logout() {
    localStorage.removeItem('user');
  }

  register(user) {
    return axios.post(API_URL + 'signup', {
      username: user.username,
      email: user.email,
      password: user.password
    });
  }
}

export default new AuthService();
